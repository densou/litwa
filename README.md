# Recruitment task



## Starting point
We've proved the following starting point

- Makefile
- docker-compose
- nginx
- php
- Symfony skeleton

```sh
make
```
Will start the server, and serve at `localhost:8000`.


```sh
make composer-update
```
Will run composer update.

## What we want

### _An exchange rate app_
_Request current exchange rate and see history of that rate, based on previous queries._



Our app should be able to handle INR/EUR rate.
The app should be easy to extend with other rates.

All queries via our app should be saved and used to display the history of a current exchange rate.

Use a 3rd party API that offers exchange rate data/services - inspiration can be from this
[list of exchange rate APIs](https://github.com/public-apis/public-apis#currency-exchange).

## Topics to showcase
* Code style
* Docker
* App design
* PHP
* Symfony
* Database
* Javascript
* JS frameworks

It's not required to use all of the above list. Showcase the ones that you feel is relevant for our exchange rate app.

## Requirements
* Incremental commits, for us to be able to see thinking and working progress. Five at least.
* A document that describes the app (how to run, how it works) and the posibilites for extending/improving the app.

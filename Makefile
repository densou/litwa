.DEFAULT_GOAL := up

up: 
	docker-compose up

composer-update:
	docker run --rm --interactive --tty --volume $(PWD):/app composer update

